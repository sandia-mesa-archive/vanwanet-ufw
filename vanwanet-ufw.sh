#!/bin/sh

# We won't need this unless VanwaNet decides to have more IP addresses.
# curl -s https://vanwanet.com/ips-v4 -o /tmp/vn_ips
# curl -s https://vanwanet.com/ips-v6 >> /tmp/vn_ips

# Allow all traffic from VanwaNet IPs (no ports restriction)
ufw allow proto tcp from 203.28.246.1 comment 'VanwaNet IP'
ufw allow proto tcp from 203.28.246.2 comment 'VanwaNet IP'

ufw reload > /dev/null

# OTHER EXAMPLE RULES
# Retrict to port 80
# ufw allow proto tcp from 203.28.246.1 to any port 80 comment 'VanwaNet IP'
# ufw allow proto tcp from 203.28.246.2 to any port 80 comment 'VanwaNet IP'

# Restrict to port 443
# ufw allow proto tcp from 203.28.246.1 to any port 443 comment 'VanwaNet IP'
# ufw allow proto tcp from 203.28.246.2 to any port 443 comment 'VanwaNet IP'

# Restrict to ports 80 & 443
# ufw allow proto tcp from 203.28.246.1 to any port 80,443 comment 'VanwaNet IP'
# ufw allow proto tcp from 203.28.246.2 to any port 80,443 comment 'VanwaNet IP'
